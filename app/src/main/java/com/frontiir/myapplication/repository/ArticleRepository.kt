package com.frontiir.myapplication.repository

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.frontiir.myapplication.model.ArticleResponse
import com.frontiir.myapplication.retrofit.ApiRequest
import com.frontiir.myapplication.retrofit.RetrofitRequest.retrofitInstance
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ArticleRepository {
    private val apiRequest: ApiRequest
    fun getMovieArticles(query: String?, key: String?): LiveData<ArticleResponse?> {

        val data = MutableLiveData<ArticleResponse?>()

        apiRequest.getMovieArticles(query, key)?.enqueue(object : Callback<ArticleResponse?> {
                override fun onResponse(call: Call<ArticleResponse?>, response: Response<ArticleResponse?>) {
                    Log.d(TAG, "onResponse response:: $response")
                    if (response.body() != null) {
                        data.setValue(response.body())
                    }
                }
                    override fun onFailure(call: Call<ArticleResponse?>, t: Throwable) {
                    data.setValue(null)
                    }
            })
        return data
    }

    companion object {
        private val TAG = ArticleRepository::class.java.simpleName
    }

    init {
        apiRequest = retrofitInstance!!.create(ApiRequest::class.java)
    }
}