package com.frontiir.myapplication.ui.home

import android.content.IntentFilter
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.frontiir.myapplication.R
import com.frontiir.myapplication.adapter.MovieArticleAdapter
import com.frontiir.myapplication.model.Article
import com.frontiir.myapplication.model.ArticleResponse
import com.frontiir.myapplication.utils.ConnectivityBroadcastReceiver
import com.frontiir.myapplication.utils.NetworkConnection
import com.google.android.material.snackbar.Snackbar
import java.util.*

class HomeFragment : Fragment() {
    private var my_recycler_view: RecyclerView? = null
    private var progress_circular_movie_article: ProgressBar? = null
    private var layoutManager: LinearLayoutManager? = null
    private var adapter: MovieArticleAdapter? = null
    private val articleArrayList = ArrayList<Article>()
    var articleViewModel: ArticleViewModel? = null

    private var mConnectivitySnackbar: Snackbar? = null
    private var mConnectivityBroadcastReceiver: ConnectivityBroadcastReceiver? = null
    private var isBroadcastReceiverRegistered: Boolean = false
    private var isFragmentLoaded: Boolean = false

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val root = inflater.inflate(R.layout.fragment_home, container, false)
        return root

    }
    @RequiresApi(Build.VERSION_CODES.N)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initailization()


    }
    override fun onStart() {
        super.onStart()

        movieArticles
    }

    fun initailization(){
        progress_circular_movie_article = view?.findViewById<View>(R.id.progress_circular_movie_article) as ProgressBar
        my_recycler_view = view!!.findViewById<View>(R.id.my_recycler_view) as RecyclerView
        // use a linear layout manager
        layoutManager = LinearLayoutManager(requireContext())
        my_recycler_view!!.layoutManager = layoutManager

        // use a linear layout manager
        // use a linear layout manager
        my_recycler_view?.setLayoutManager(layoutManager)

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        my_recycler_view?.setHasFixedSize(true)

        // adapter
        // adapter
        adapter = MovieArticleAdapter(this@HomeFragment, articleArrayList)
        my_recycler_view?.setAdapter(adapter)

        // View Model
        // View Model
        articleViewModel = ViewModelProviders.of(this)[ArticleViewModel::class.java]

    }

    private val movieArticles: Unit
        get() {
            articleViewModel!!.articleResponseLiveData.observe(this, androidx.lifecycle.Observer { article: ArticleResponse?->
               if (article!=null){
                   progress_circular_movie_article!!.visibility=View.GONE
                   val articles = article.articles
                    val addAll = articles?.let { articleArrayList.addAll(it) }
                   adapter!!.notifyDataSetChanged()
               }

            })

        }


    companion object {
        private val TAG = HomeFragment::class.java.simpleName
    }
//    override fun onResume() {
//        super.onResume()
//
//        if (!isFragmentLoaded && !NetworkConnection.isConnected(requireContext())) {
//            mConnectivitySnackbar = Snackbar.make(activity?.findViewById(R.id.nav_host_fragment)!!,
//                R.string.no_network, Snackbar.LENGTH_INDEFINITE)
//            mConnectivitySnackbar!!.show()
//            mConnectivityBroadcastReceiver = ConnectivityBroadcastReceiver(object :
//                ConnectivityBroadcastReceiver.ConnectivityReceiverListener {
//                override fun onNetworkConnectionConnected() {
//                    mConnectivitySnackbar!!.dismiss()
//                    isFragmentLoaded = true
//                    movieArticles
//                    isBroadcastReceiverRegistered = false
//                    activity?.unregisterReceiver(mConnectivityBroadcastReceiver)
//                }
//            })
//            val intentFilter = IntentFilter("android.net.conn.CONNECTIVITY_CHANGE")
//            isBroadcastReceiverRegistered = true
//            activity?.registerReceiver(mConnectivityBroadcastReceiver, intentFilter)
//        } else if (!isFragmentLoaded && NetworkConnection.isConnected(requireContext())) {
//            isFragmentLoaded = true
//            movieArticles
//        }
//    }

}
