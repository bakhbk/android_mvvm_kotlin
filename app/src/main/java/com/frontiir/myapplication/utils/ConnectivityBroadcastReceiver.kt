package com.frontiir.myapplication.utils

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
/**
 * Created by hitanshu on 26/8/17.
 */

class ConnectivityBroadcastReceiver(private val mConnectivityReceiverListener: ConnectivityReceiverListener?) : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        if (mConnectivityReceiverListener != null && NetworkConnection.isConnected(context))
            mConnectivityReceiverListener.onNetworkConnectionConnected()
    }


    interface ConnectivityReceiverListener {
        fun onNetworkConnectionConnected()
    }

}
