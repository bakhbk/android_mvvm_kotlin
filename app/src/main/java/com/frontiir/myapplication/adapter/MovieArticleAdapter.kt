package com.frontiir.myapplication.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.frontiir.myapplication.R
import com.frontiir.myapplication.model.Article
import com.frontiir.myapplication.ui.home.HomeFragment
import java.util.*

class MovieArticleAdapter(private val context: HomeFragment, var articleArrayList: ArrayList<Article>)
    : RecyclerView.Adapter<MovieArticleAdapter.ViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ViewHolder {
        val view = LayoutInflater.from(viewGroup.context).inflate(R.layout.list_each_row_movie_article, viewGroup, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, i: Int) {
        val article = articleArrayList[i]
        viewHolder.tvTitle.text = article.title
        viewHolder.tvAuthorAndPublishedAt.text = "-" + article.author + " | " + "Piblishetd At: " + article.publishedAt
        viewHolder.tvDescription.text = article.description
        Glide.with(context)
            .load(article.urlToImage)
            .into(viewHolder.imgViewCover)
    }

    override fun getItemCount(): Int {
        return articleArrayList.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val imgViewCover: ImageView
        val tvTitle: TextView
        val tvAuthorAndPublishedAt: TextView
        val tvDescription: TextView

        init {
            imgViewCover = itemView.findViewById<View>(R.id.imgViewCover) as ImageView
            tvTitle = itemView.findViewById<View>(R.id.tvTitle) as TextView
            tvAuthorAndPublishedAt = itemView.findViewById<View>(R.id.tvAuthorAndPublishedAt) as TextView
            tvDescription = itemView.findViewById<View>(R.id.tvDescription) as TextView
        }
    }

}