package com.frontiir.myapplication.model

class Article {

    var title: String? = null
    var author: String? = null
    var urlToImage: String? = null
    var publishedAt: String? = null
    var description: String? = null

}